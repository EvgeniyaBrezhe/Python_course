import os


def traverse_dir_2(heads_list, total_size=0):
    if len(heads_list) > 0:
        head, tail = heads_list[0:len(heads_list) - 1], heads_list[len(heads_list) - 1]
    else:
        return total_size
    if tail:
        global my_path
        path = os.path.join(my_path, tail)
        total_size += os.path.getsize(path)
        return traverse_dir_2(head, total_size)


my_path = "/Users/popovaevgeniya/Downloads/Test_Folder"
initial_list_for_function = os.listdir(my_path)
dir_size_2 = traverse_dir_2(initial_list_for_function)
print(dir_size_2)


def traverse_dir_3(initial_list):
    # if len(heads_list) > 0:
    #     head, tail = heads_list[0:len(heads_list) - 1], heads_list[len(heads_list) - 1]
    # else:
    #     return total_size
    head, tail, total_size = initial_list[:1], initial_list[1:], 0
    while tail:
        head, tail = initial_list[:1], initial_list[1:]
        global my_path
        path = os.path.join(my_path, head[0])
        initial_list = tail
        total_size = os.path.getsize(path) + total_size
    return total_size


my_path = "/Users/popovaevgeniya/Downloads/Test_Folder"
initial_list_for_function = os.listdir(my_path)
dir_size_3 = traverse_dir_3(initial_list_for_function)
print(dir_size_3)
